import 'dart:async';

import 'package:app_do_vui_hai_nao/src/resource/model/model.dart';
import 'package:rx_shared_preferences/rx_shared_preferences.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

class AppShared {
  AppShared._();

  static final prefs = RxSharedPreferences(SharedPreferences.getInstance());

  static const String keyAccessToken = "keyViSafeAccessToken";
  static const String keyLife = "Life";
  static const String keyLevel = "Level";
  static const String keyAnswers = "Answers";
  static const String keyQuestion = "Question";
  static const String keySounds = "Sounds";

  static Future<bool> setAccessToken(String token) =>
      prefs.setString(keyAccessToken, token);

  static Future<String> getAccessToken() => prefs.getString(keyAccessToken);

  static Future<bool> setLife(int value) => prefs.setInt(keyLife, value);

  static Future<int> getLife() async {
    int value = await prefs.getInt(keyLife);
    if (value != null) {
      return value;
    } else {
      return 0;
    }
  }

  static Future<bool> setLevel(int value) => prefs.setInt(keyLevel, value);

  static Future<int> getLevel() async {
    int value = await prefs.getInt(keyLevel);
    if (value != null) {
      return value;
    } else {
      return 1;
    }
  }

  static Future<bool> setAnswers(List<Quest> value) {
    String val = value != null ? json.encode(value) : "";
    prefs.setString(keyAnswers, val);
  }

  static Future<List<Quest>> getAnswers() async {
    String val = await prefs.getString(keyAnswers);
    List<Quest> value = [];
    if (val != null) {
      List<dynamic> data = json.decode(val);
      for (var item in data) {
        value.add(Quest.fromJson(item));
      }
    }
    return value;
  }

  static Future<bool> setQuestion(List<Quest> value) {
    String val = value != null ? json.encode(value) : "";
    prefs.setString(keyQuestion, val);
  }

  static Future<List<Quest>> getQuestion() async {
    String val = await prefs.getString(keyQuestion);
    List<Quest> value = [];
    if (val != null) {
      List<dynamic> data = json.decode(val);
      for (var item in data) {
        value.add(Quest.fromJson(item));
      }
    }
    return value;
  }

  static Future<bool> getSound() async{
    bool check = await prefs.getBool(keySounds);
    if (check == null) return true;
    return check;
  }

  static Future<bool> setSound(bool value) => prefs.setBool(keySounds, value);

}
