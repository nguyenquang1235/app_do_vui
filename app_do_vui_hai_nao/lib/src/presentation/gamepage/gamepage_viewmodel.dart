import 'package:app_do_vui_hai_nao/src/presentation/presentation.dart';
import 'package:app_do_vui_hai_nao/src/resource/model/model.dart';
import 'package:app_do_vui_hai_nao/src/resource/repo/quest_repository.dart';
import 'package:app_do_vui_hai_nao/src/resource/resource.dart';
import 'package:app_do_vui_hai_nao/src/utils/app_shared.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import 'package:toast/toast.dart';

class GamePageViewModel extends BaseViewModel {
  final QuestRepository repository;
  final SoundService soundService;

  List<Quest> _listQuests;
  List<Quest> _listAnswered;
  int _life;
  int _level;
  Quest _quest;

  final _questionController = BehaviorSubject<Quest>();
  final _textQuest = BehaviorSubject<String>();
  final _showCorrect = BehaviorSubject<String>();
  final _timeController = BehaviorSubject<int>();

  Stream<Quest> get questionStream => _questionController.stream;
  Sink<Quest> get questionSink => _questionController.sink;

  Stream<String> get textQuestStream => _textQuest.stream;
  Sink<String> get textQuestSink => _textQuest.sink;

  Stream<String> get showCorrectStream => _showCorrect.stream;
  Sink<String> get showCorrectSink => _showCorrect.sink;

  Stream<int> get timeStream => _timeController.stream;
  Sink<int> get timeSink => _timeController.sink;

  GamePageViewModel({@required this.repository, @required this.soundService});

  init() async {
    //Lấy câu hỏi từ bộ nhớ Cache
    _listQuests = await AppShared.getQuestion();
    _listAnswered = await AppShared.getAnswers();
    if (await getLife() <= 1) {
      await resetLife();
    }
    await getLevel();
    getTime();
    await resetQuestion();
  }

  getLife() async {
    _life = await AppShared.getLife();
    return _life;
  }

  resetLife() async {
    AppShared.setLife(5);
  }

  checkLife() async {
    if (_life <= 1) {
      resetLife();
      resetLevel();
    } else {
      _life += -1;
      await AppShared.setLife(_life);
    }
  }

  Future<int> getLevel() async {
    return _level = await AppShared.getLevel();
  }

  resetLevel() {
    _level = 1;
    AppShared.setLevel(_level);
  }

  checkLevel() async {
    // if(_level == 1){
    //   _time = 60;
    // } if(_level % 10 == 0){
    //   _time -= 5;
    // }
    // print(_level);
    // print(_time);
  }

  getTime(){
    int multiple = _level ~/ 2;
    int time = 60 - multiple*5;
    timeSink.add(time);
  }

  resetQuestion() async {
    _listQuests.shuffle();
    _quest = _listQuests[0];
    //Lấy Quest đầu tiên để đẩy vào Stream
    questionSink.add(_quest);
    subStringText();
    getTime();
  }

  removeResetQuest(bool check) async {
    //neu tra loi dung
    if (check) {
      _listQuests.remove(_quest);
      await AppShared.setQuestion(_listQuests);
      _listAnswered.add(_quest);
      await AppShared.setAnswers(_listAnswered);
      _level += 1;
      await AppShared.setLevel(_level);
      String wifiMessage = await addMoreQuest();

      if (wifiMessage != null) {
        return wifiMessage;
      }
    } else {
      await checkLife();
    }
    await resetQuestion();
    return null;
  }

  addMoreQuest() async {
    if (_listQuests.isEmpty) {
      var data = await repository.getQuest(_listAnswered.length + 1);
      if (!await checkWifi(data)) {
        _listQuests = data.data.questions;
        await AppShared.setQuestion(_listQuests);
      }
      return data.message;
    }
    return null;
  }

  Future<bool>checkWifi(NetworkState<QuestApplication> data) async {
    if (data.message != null) {
      Toast.show(data.message, context, duration: 3);
    }
    await Future.delayed(Duration(seconds: 3));
    return data.message != null;
  }

  // subString
  subStringText() async {
    List<String> text = _quest.cauHoi.split("");
    String result = "";
    for (var item in text) {
      await Future.delayed(
        Duration(milliseconds: 4),
        () {
          result += item;
          textQuestSink.add(result);
        },
      );
    }
  }

  pushCorrect() async {
    showCorrectSink.add(_quest.a);
    await Future.delayed(Duration(milliseconds: 500));
    showCorrectSink.add("");
  }

  playSound(GameSound gameSound) async {
    await soundService.playSound(gameSound);
  }

}
