import 'dart:math';

import 'package:app_do_vui_hai_nao/src/configs/configs.dart';
import 'package:app_do_vui_hai_nao/src/presentation/presentation.dart';
import 'package:app_do_vui_hai_nao/src/resource/model/model.dart';
import 'package:app_do_vui_hai_nao/src/resource/services/sounds_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:screenshot/screenshot.dart';
import 'package:provider/provider.dart';
import 'package:share_extend/share_extend.dart';
import 'package:path_provider/path_provider.dart';
import 'package:toast/toast.dart';

class GamePageScreen extends StatefulWidget {
  @override
  _GamePageScreenState createState() => _GamePageScreenState();
}

class _GamePageScreenState extends State<GamePageScreen>
    with TickerProviderStateMixin {
  int _life;
  int _level;
  Quest _quest;
  GamePageViewModel _viewModel;
  bool checkCorrect = false;
  final screenshotController = ScreenshotController();
  AnimationController _victoryController;
  AnimationController _starsController;
  AnimationController _lostController;
  AnimationController _boardController;
  AnimationController _dialogController;

  @override
  void dispose() {
    _viewModel.dispose();
    if (_victoryController != null || _lostController != null) {
      closeAnimation(checkCorrect: checkCorrect, isCancel: IsCancel.Dispose);
    }
    _boardController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    WidgetConfigSize().init(context);
    return BaseWidget<GamePageViewModel>(
      viewModel: GamePageViewModel(
        repository: Provider.of(context),
        soundService: Provider.of(context),
      ),
      onViewModelReady: (viewModel) async {
        _viewModel = viewModel;
        await _viewModel.init();
        _boardController = AnimationController(
            duration: Duration(milliseconds: 700), vsync: this);
        _boardController.forward();
      },
      builder: (context, viewModel, child) => Scaffold(
        body: SafeArea(
          child: Screenshot(
            controller: screenshotController,
            child: StreamBuilder(
              stream: _viewModel.questionStream,
              builder: (context, snapshot) {
                return _checkDataSnapshot(snapshot);
              },
            ),
          ),
        ),
      ),
    );
  }

  Widget _checkDataSnapshot(snapshot) {
    if (snapshot.connectionState == ConnectionState.waiting) {
      return WidgetLoadingPage();
    } else {
      _quest = snapshot.data;
      checkCorrect = false;
      return _buildGamePage();
    }
  }

  Widget _buildGamePage() {
    return Stack(
      children: [
        _buildBody(),
        _buildHeader(),
      ],
    );
  }

  Widget _buildHeader() {
    double ratio = MediaQuery.of(context).size.width > 500 ? 7 : 6;
    return AspectRatio(
      aspectRatio: ratio,
      child: Stack(
        children: [
          Container(
            padding: const EdgeInsets.only(bottom: 12),
            decoration: const BoxDecoration(
              image: DecorationImage(
                  image: AssetImage(AppImages.gameBar1), fit: BoxFit.fill),
            ),
            child: Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  WidgetGameButton(
                    onTap: () async {
                      await _viewModel.playSound(GameSound.Back);
                      Navigator.pop(context, true);
                    },
                    normal: Image.asset(
                      AppImages.btnBack1,
                      width: WidgetConfigSize.blockSizeHorizontal * 15,
                      height: WidgetConfigSize.blockSizeHorizontal * 15,
                    ),
                    hover: Image.asset(
                      AppImages.btnBack2,
                      width: WidgetConfigSize.blockSizeHorizontal * 15,
                      height: WidgetConfigSize.blockSizeHorizontal * 15,
                    ),
                  ),
                  Align(
                    alignment: Alignment(0, 0.5),
                    child: _buildHeartAndLife(),
                  ),
                  WidgetGameButton(
                    onTap: () async {
                      await _viewModel.playSound(GameSound.Back);
                      shareQuestion();
                    },
                    normal: Image.asset(
                      AppImages.btnScreenshot1,
                      width: WidgetConfigSize.blockSizeHorizontal * 15,
                      height: WidgetConfigSize.blockSizeHorizontal * 15,
                    ),
                    hover: Image.asset(
                      AppImages.btnScreenshot2,
                      width: WidgetConfigSize.blockSizeHorizontal * 15,
                      height: WidgetConfigSize.blockSizeHorizontal * 15,
                    ),
                  )
                ],
              ),
            ),
          ),
          Align(
              alignment: Alignment(-0.68, -0.3),
              child: Transform.rotate(angle: pi, child: _buildTimeIndicator())),
          Align(
              alignment: Alignment(0.667, -0.3),
              child: Transform(
                  alignment: Alignment.center,
                  transform: Matrix4.rotationX(pi),
                  child: _buildTimeIndicator())),
        ],
      ),
    );
  }

  Widget _buildTimeIndicator() {
    return StreamBuilder(
        stream: _viewModel.timeStream,
        builder: (context, snapshot) {
          if(snapshot.connectionState == ConnectionState.waiting){
            return Container();
          }
          else{
            return WidgetTimeIndicator(
              time: snapshot.data,
            );
          }
        });
  }

  Widget _buildBody() {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      decoration: const BoxDecoration(
          image: DecorationImage(
              image: AssetImage(AppImages.background2), fit: BoxFit.fill)),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Expanded(
            child: Opacity(
              child: Text("ádasdasd"),
              opacity: 0,
            ),
            flex: 1,
          ),
          Expanded(
            child: _buildBoard(),
            flex: 3,
          ),
          Expanded(
            child: Container(
              child: Center(
                child: AspectRatio(
                  aspectRatio:
                      MediaQuery.of(context).size.width > 500 ? 1.45 : 1.075,
                  child: _buildColumnAnswerBar(),
                ),
              ),
            ),
            flex: 4,
          ),
          Expanded(
            child: Opacity(
              child: Text("ádasdasd"),
              opacity: 0,
            ),
            flex: 1,
          ),
        ],
      ),
    );
  }

  Widget _buildBoard() {
    var _offsetAnimation = Tween<Offset>(
      begin: Offset(-1, 0),
      end: Offset(0, 0),
    ).animate(CurvedAnimation(
      parent: _boardController,
      curve: Curves.easeInCubic,
    ));
    return AnimatedBuilder(
      animation: _offsetAnimation,
      builder: (context, child) {
        return SlideTransition(
          position: _offsetAnimation,
          child: child,
        );
      },
      child: Stack(
        children: [
          Center(
            child: AspectRatio(
              aspectRatio: 5 / 3,
              child: Container(
                // width: MediaQuery.of(context).size.width,
                child: Image.asset(
                  AppImages.board1,
                  fit: BoxFit.fill,
                ),
              ),
            ),
          ),
          Center(
            child: AspectRatio(
              aspectRatio: 1.25,
              child: Container(
                margin: const EdgeInsets.only(
                    left: 16, right: 16, bottom: 12, top: 24),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Expanded(
                      child: SizedBox(),
                      flex: 1,
                    ),
                    Expanded(
                      flex: 5,
                      child: FutureBuilder(
                        future: _viewModel.getLevel(),
                        builder: (context, snapshot) {
                          _level = snapshot.data;
                          return Center(
                            child: Text(
                              "Cấp độ $_level",
                              style: AppStyles.DEFAULT_VERY_LARGE_BOLD.copyWith(
                                  color: Colors.yellow,
                                  fontFamily: "Sunshiney"),
                            ),
                          );
                        },
                      ),
                    ),
                    Expanded(
                      flex: 7,
                      child: SingleChildScrollView(
                        child: StreamBuilder(
                          stream: _viewModel.textQuestStream,
                          builder: (context, snapshot) {
                            if (snapshot.connectionState ==
                                ConnectionState.waiting) {
                              return Opacity(
                                opacity: 0,
                                child: Text("ádasdsad"),
                              );
                            } else {
                              return Text(
                                snapshot.data,
                                style: AppStyles.DEFAULT_MEDIUM_BOLD
                                    .copyWith(color: Colors.orange),
                              );
                            }
                          },
                        ),
                      ),
                    ),
                    Expanded(
                      child: SizedBox(),
                      flex: 2,
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildColumnAnswerBar() {
    List<String> listAnswer = _shuffleAnswer();
    var _offsetAnimation = Tween<Offset>(
      begin: Offset(0, 1),
      end: Offset(0, 0),
    ).animate(CurvedAnimation(
      parent: _boardController,
      curve: Curves.easeInCubic,
    ));
    return AnimatedBuilder(
      animation: _offsetAnimation,
      builder: (context, child) {
        return SlideTransition(
          position: _offsetAnimation,
          child: child,
        );
      },
      child: StreamBuilder<Object>(
          stream: _viewModel.showCorrectStream,
          builder: (context, snapshot) {
            return Column(
              children: [
                Expanded(
                  child: SizedBox(
                    height: 12,
                  ),
                ),
                Expanded(
                  child: WidgetAnswerBar(
                    choose: AppImages.choose1,
                    text: listAnswer[0],
                    answerCorrect: _quest.a,
                    paramFunction: (value) async {
                      await confirmAnswer(value);
                    },
                    showCorrect: snapshot.data,
                  ),
                ),
                Expanded(
                  child: WidgetAnswerBar(
                    choose: AppImages.choose2,
                    text: listAnswer[1],
                    answerCorrect: _quest.a,
                    paramFunction: (value) async {
                      await confirmAnswer(value);
                    },
                    showCorrect: snapshot.data,
                  ),
                ),
                Expanded(
                  child: WidgetAnswerBar(
                    choose: AppImages.choose3,
                    text: listAnswer[2],
                    answerCorrect: _quest.a,
                    paramFunction: (value) async {
                      await confirmAnswer(value);
                    },
                    showCorrect: snapshot.data,
                  ),
                ),
                Expanded(
                  child: WidgetAnswerBar(
                    choose: AppImages.choose4,
                    text: listAnswer[3],
                    answerCorrect: _quest.a,
                    paramFunction: (value) async {
                      await confirmAnswer(value);
                    },
                    showCorrect: snapshot.data,
                  ),
                ),
              ],
            );
          }),
    );
  }

  Widget _buildHeartAndLife() {
    var _offsetAnimation = Tween<Offset>(
      begin: Offset(0, -3),
      end: Offset(0, 0),
    ).animate(CurvedAnimation(
      parent: _boardController,
      curve: Curves.easeInCubic,
    ));
    return AnimatedBuilder(
      animation: _offsetAnimation,
      builder: (context, child) {
        return SlideTransition(
          position: _offsetAnimation,
          child: child,
        );
      },
      child: FutureBuilder(
        future: _viewModel.getLife(),
        builder: (context, snapshot) {
          _life = snapshot.data;
          return Row(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.baseline,
            children: [
              Image.asset(
                AppImages.heart,
                height: 35,
                width: 35,
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                "$_life",
                style: AppStyles.DEFAULT_VERY_LARGE_BOLD
                    .copyWith(color: Colors.white, fontFamily: "Itim"),
              )
            ],
          );
        },
      ),
    );
  }

  List<String> _shuffleAnswer() {
    List<String> listAnswer = [];
    listAnswer.add(_quest.a);
    listAnswer.add(_quest.b);
    listAnswer.add(_quest.c);
    listAnswer.add(_quest.d);
    listAnswer.shuffle();
    return listAnswer;
  }

  _showDialogCorrect() {
    _dialogController = AnimationController(
      duration: const Duration(seconds: 1),
      vsync: this,
    );
    _dialogController.forward();
    Animation<double> _animation = CurvedAnimation(
      parent: _dialogController,
      curve: Curves.fastOutSlowIn,
    );
    Widget child;
    checkCorrect ? child = _buildVictoryDialog() : child = _buildLossDialog();
    return showDialog(
      context: context,
      builder: (context) => StatefulBuilder(
        builder: (context, setState) {
          return Material(
            type: MaterialType.transparency,
            child: Container(
              height: WidgetConfigSize.screenHeight,
              color: Colors.black.withOpacity(0.25),
              alignment: Alignment(0, 0),
              child: FadeTransition(
                child: child,
                opacity: _animation,
              ),
            ),
          );
        },
      ),
    ).then((value) async {
      await _viewModel.playSound(GameSound.Back);
      closeAnimation(checkCorrect: checkCorrect, isCancel: IsCancel.Stop);
      if (await _viewModel.removeResetQuest(checkCorrect) != null) {
        Navigator.pop(context, true);
      }
    });
  }

  Widget _buildVictoryDialog() {
    return Stack(
      children: [
        _buildStarAnimation(),
        Align(
          alignment: Alignment(0, -0.65),
          child: Container(
            width: WidgetConfigSize.blockSizeHorizontal * 50,
            height: WidgetConfigSize.blockSizeHorizontal * 50,
            child: Image.asset(
              AppImages.victoryFace,
              fit: BoxFit.fill,
            ),
          ),
        ),
        Positioned(
          left: WidgetConfigSize.blockSizeHorizontal * 2,
          top: WidgetConfigSize.blockSizeVertical * 7,
          child: _buildVictoryAnimation(),
        ),
        Positioned(
          top: WidgetConfigSize.blockSizeVertical * 35,
          child: _buildBodyDialog(),
        ),
        Positioned(
          top: WidgetConfigSize.blockSizeVertical * 56,
          // top: WidgetConfigSize.blockSizeVertical * 20,
          left: WidgetConfigSize.blockSizeHorizontal * 36,
          child: _buildContinueButton(),
        ),
      ],
    );
  }

  Widget _buildVictoryAnimation() {
    Animation<double> _animation;
    _victoryController = AnimationController(
        duration: Duration(milliseconds: 600),
        vsync: this,
        lowerBound: 0.35,
        upperBound: 0.6);
    _victoryController.repeat(reverse: true);
    _animation = CurvedAnimation(
      parent: _victoryController,
      curve: Curves.fastOutSlowIn,
    );

    return Container(
      width: WidgetConfigSize.blockSizeHorizontal * 40,
      height: WidgetConfigSize.blockSizeVertical * 30,
      child: AnimatedBuilder(
        animation: _animation,
        builder: (context, child) {
          return ScaleTransition(
            alignment: Alignment(0.5, 0),
            scale: _animation,
            child: child,
          );
        },
        child: Image.asset(
          AppImages.victoryHand,
          fit: BoxFit.fill,
        ),
      ),
    );
  }

  Widget _buildStarAnimation() {
    List<Widget> images = [
      Image.asset(
        AppImages.stars1,
        fit: BoxFit.fill,
      ),
      Image.asset(AppImages.stars2, fit: BoxFit.fill)
    ];
    Widget currentChild1 = images[0];
    _starsController = AnimationController(
        vsync: this,
        duration: Duration(milliseconds: 300),
        lowerBound: 0,
        upperBound: 5);
    _starsController.repeat(reverse: true);
    _starsController.addListener(() {
      currentChild1 = _starsController.view.value < 2.5 ? images[0] : images[1];
    });
    return AnimatedBuilder(
      animation: _starsController,
      builder: (context, child) => Container(
        width: WidgetConfigSize.screenWidth,
        child: currentChild1,
      ),
    );
  }

  Widget _buildLossDialog() {
    return Stack(
      children: [
        Positioned(
          top: WidgetConfigSize.blockSizeVertical * 10,
          child: Column(
            children: [
              Container(
                width: WidgetConfigSize.blockSizeHorizontal * 50,
                height: WidgetConfigSize.blockSizeVertical * 25,
                child: Image.asset(
                  AppImages.loseFace,
                  fit: BoxFit.fill,
                ),
              ),
              _buildBodyDialog(),
            ],
          ),
        ),
        Positioned(
          left: WidgetConfigSize.blockSizeHorizontal * 25,
          top: WidgetConfigSize.blockSizeVertical * 12,
          child: _buildLossAnimation(),
        ),
        Positioned(
          top: WidgetConfigSize.blockSizeVertical * 56,
          left: WidgetConfigSize.blockSizeHorizontal * 36,
          child: _buildContinueButton(),
        ),
      ],
    );
  }

  Widget _buildLossAnimation() {
    _lostController = AnimationController(
        duration: Duration(milliseconds: 600),
        vsync: this,
        lowerBound: 0.3,
        upperBound: 0.5);
    _lostController.repeat(reverse: true);
    var _offsetAnimation = Tween<Offset>(
      begin: Offset.zero,
      end: Offset(-0.6, -0.2),
    ).animate(CurvedAnimation(
      parent: _lostController,
      curve: Curves.easeInOut,
    ));
    return Container(
      width: WidgetConfigSize.blockSizeHorizontal * 20,
      height: WidgetConfigSize.blockSizeVertical * 10,
      child: AnimatedBuilder(
        animation: _lostController,
        builder: (context, child) {
          return SlideTransition(
            position: _offsetAnimation,
            child: child,
          );
        },
        child: Image.asset(
          AppImages.loseHand,
          fit: BoxFit.fill,
        ),
      ),
    );
  }

  Widget _buildBodyDialog() {
    return Container(
        padding: EdgeInsets.symmetric(
            horizontal: WidgetConfigSize.blockSizeHorizontal * 4,
            vertical: WidgetConfigSize.blockSizeHorizontal * 6),
        width: WidgetConfigSize.screenWidth,
        height: WidgetConfigSize.screenHeight / 4,
        decoration: const BoxDecoration(
            image: DecorationImage(
                image: AssetImage(AppImages.board2), fit: BoxFit.fill)),
        child: SingleChildScrollView(
          child: Text(
            _quest.giaiThich,
            style: AppStyles.DEFAULT_LARGE_BOLD.copyWith(
              fontFamily: "Itim",
              letterSpacing: 1.5,
              fontSize: 18,
            ),
          ),
          scrollDirection: Axis.vertical,
        ));
  }

  Widget _buildContinueButton() {
    return Container(
      width: WidgetConfigSize.blockSizeHorizontal * 30,
      height: WidgetConfigSize.blockSizeVertical * 7,
      child: WidgetGameButton(
        normal: Image.asset(AppImages.continue1),
        hover: Image.asset(AppImages.continue2),
        onTap: () {
          Navigator.pop(context);
        },
      ),
    );
  }

  confirmAnswer(value) async {
    checkCorrect = value;
    _viewModel.pushCorrect();
    if (!value) {
      await _viewModel.playSound(GameSound.Loss);
    } else {
      await _viewModel.playSound(GameSound.Right);
    }
    Future.delayed(Duration(milliseconds: 275), () => _showDialogCorrect());
  }

  _takeScreenShot() async {
    final directory = (await getApplicationDocumentsDirectory()).path;
    final time = DateTime.now().millisecondsSinceEpoch;
    final path = '$directory/screenshot_dovuihainao_$time.png';
    print(path);
    screenshotController.capture(path: path).then(
      (image) async {
        await ImageGallerySaver.saveImage(image.readAsBytesSync());
        ShareExtend.share(image.path, 'image');
      },
    );
  }

  void shareQuestion() async {
    var status = await Permission.storage.status;
    if (status.isGranted) {
      Toast.show("Đang xử lý...", context, duration: Toast.LENGTH_SHORT);
      await _takeScreenShot();
    } else if (status.isDenied)
      Toast.show("Không thể lưu", context, duration: Toast.LENGTH_SHORT);
    else {
      await Permission.storage.request();
      shareQuestion();
    }
  }

  void closeAnimation({bool checkCorrect, IsCancel isCancel}) async {
    switch (isCancel) {
      case IsCancel.Stop:
        if (!checkCorrect) {
          _lostController.stop();
        } else {
          _victoryController.stop();
          _starsController.stop();
        }
        break;
      case IsCancel.Dispose:
        if (!checkCorrect && _victoryController != null) {
          _victoryController.dispose();
          _starsController.dispose();
        } else {
          _lostController.dispose();
        }
        break;
    }
  }
}

enum IsCancel {
  Stop,
  Dispose,
}
