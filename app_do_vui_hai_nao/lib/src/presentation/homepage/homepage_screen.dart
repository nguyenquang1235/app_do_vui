import 'package:app_do_vui_hai_nao/src/configs/configs.dart';
import 'package:app_do_vui_hai_nao/src/presentation/base/base.dart';
import 'package:app_do_vui_hai_nao/src/presentation/homepage/homepage.dart';
import 'package:app_do_vui_hai_nao/src/presentation/presentation.dart';
import 'package:app_do_vui_hai_nao/src/resource/services/sounds_service.dart';
import 'package:flutter/material.dart';
import 'package:launch_review/launch_review.dart';
import 'package:provider/provider.dart';
import 'package:share/share.dart';

class HomePageScreen extends StatefulWidget {
  @override
  _HomePageScreenState createState() => _HomePageScreenState();
}

class _HomePageScreenState extends State<HomePageScreen> {
  int level;
  HomePageViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    WidgetConfigSize().init(context);
    return BaseWidget<HomePageViewModel>(
      viewModel: HomePageViewModel(
          repository: Provider.of(context), soundService: Provider.of(context)),
      onViewModelReady: (viewModel) async {
        _viewModel = viewModel;
        _viewModel.init();
      },
      builder: (context, viewModel, child) {
        return StreamBuilder(
          stream: viewModel.levelStream,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Scaffold(body: WidgetLoadingPage());
            } else {
              level = snapshot.data;
              return _buildBody();
            }
          },
        );
      },
    );
  }

  _buildBody() {
    return Scaffold(
      body: SafeArea(
        child: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage(AppImages.background1), fit: BoxFit.fill)),
          child: Center(
            child: Column(
              children: [
                Expanded(
                  flex: 1,
                  child: _buildTopRow(),
                ),
                Expanded(
                  flex: 3,
                  child: Image.asset(AppImages.banner),
                ),
                Expanded(
                  flex: 3,
                  child: _buildBottomAction(),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  _buildTopRow() {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 8),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Expanded(child: Container(
            width: double.maxFinite,
          )),
          Expanded(child: Row(
            children: [
              Expanded(child: SizedBox(width: 8,)),
              Expanded(child: _buildShareButton()),
              SizedBox(width: 8,),
              Expanded(child: _buildLikeButton()),
              SizedBox(width: 8,),
              Expanded(child: _onOffMusicButton()),
            ],
          ))
        ],
      ),
    );
  }

  _buildBottomAction() {
    return Column(
      children: [
        Text(
          "Cấp độ ${level}",
          style:
              TextStyle(color: Colors.yellow, fontFamily: "Itim", fontSize: 35),
        ),
        _buildPlayGameButton(),
        _buildMoreAppButton(),
      ],
    );
  }

  _onOffMusicButton() {
    return StreamBuilder(
      stream: _viewModel.isMusicStream,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting)
          return Container();
        return GestureDetector(
          child: Image.asset(
              snapshot.data ? AppImages.btnOnVolume : AppImages.btnOffVolume),
          onTap: () async {
            await _viewModel.playSound(GameSound.Back);
            await Future.delayed(Duration(milliseconds: 200), () {
              snapshot.data
                  ? _viewModel.onOffMusic(IsMusic.TurnOff)
                  : _viewModel.onOffMusic(IsMusic.TurnOn);
            });
          },
        );
      },
    );
  }

  _buildLikeButton() {
    return WidgetGameButton(
      onTap: () async {
        await _viewModel.playSound(GameSound.Back);
        LaunchReview.launch(androidAppId: "com.huesoft.dovuihainao");
      },
      hover: Image.asset(AppImages.btnLike2),
      normal: Image.asset(AppImages.btnLike1),
    );
  }

  _buildShareButton() {
    return WidgetGameButton(
      onTap: () async {
        await _viewModel.playSound(GameSound.Back);
        Share.share(
            'Chơi cái này zui lắm nè\nhttps://play.google.com/store/apps/details?id=com.huesoft.dovuihainao');
      },
      normal: Image.asset(AppImages.btnLink1),
      hover: Image.asset(AppImages.btnLink2),
    );
  }

  _buildPlayGameButton() {
    return Container(
      width: WidgetConfigSize.blockSizeHorizontal*60,
      height: WidgetConfigSize.blockSizeVertical*10,
      child: WidgetGameButton(
        onTap: () async {
          await _viewModel.playSound(GameSound.Play);
          if (!await _viewModel.requestQuest()) {
            var result = await Navigator.pushNamed(context, Routers.gamePage);
            if (result) {
              _viewModel.showLevel();
            }
          }
        },
        normal: Image.asset(AppImages.playGame1,fit: BoxFit.fill,),
        hover: Image.asset(AppImages.playGame2,fit: BoxFit.fill,),
      ),
    );
  }

  _buildMoreAppButton() {
    return Container(
      width: WidgetConfigSize.blockSizeHorizontal*60,
      height: WidgetConfigSize.blockSizeVertical*10,
      child: WidgetGameButton(
        onTap: () async {
          await _viewModel.playSound(GameSound.MoreApp);
          if (!await _viewModel.requestWifi()) {
            Navigator.pushNamed(context, Routers.moreApp);
          }
        },
        normal: Image.asset(AppImages.gameHot1, fit: BoxFit.fill,),
        hover: Image.asset(AppImages.gameHot2, fit: BoxFit.fill,),
      ),
    );
  }
}
