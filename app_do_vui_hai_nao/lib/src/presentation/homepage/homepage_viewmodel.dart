import 'package:app_do_vui_hai_nao/src/configs/configs.dart';
import 'package:app_do_vui_hai_nao/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';
import '../../resource/resource.dart';
import '../base/base.dart';
import 'package:rxdart/rxdart.dart';

import '../presentation.dart';

enum IsMusic { TurnOn, TurnOff }

class HomePageViewModel extends BaseViewModel {
  final QuestRepository repository;
  final SoundService soundService;
  final _level = BehaviorSubject<int>();
  final _isMusic = BehaviorSubject<bool>();

  Stream<int> get levelStream => _level.stream;
  Sink<int> get levelSink => _level.sink;

  Stream<bool> get isMusicStream => _isMusic.stream;
  Sink<bool> get isMusicSink => _isMusic.sink;

  HomePageViewModel({@required this.repository, @required this.soundService});

  init() async {
    _playMusic();
    isMusicSink.add(await AppShared.getSound());
    showLevel();
  }

  void showLevel() async{
    levelSink.add(await getLevel());
  }

  Future<int> getLevel() async {
    return await AppShared.getLevel();
  }

// kiem tra xem list cau hỏi đã có trong cache chưa
  checkListQuest() async {
    List<Quest> check = await AppShared.getQuestion();
    return check.isEmpty;
  }

  void onOffMusic(IsMusic isMusic) async {
    switch (isMusic) {
      case IsMusic.TurnOn:
        await AppShared.setSound(true);
        isMusicSink.add(true);
        break;
      case IsMusic.TurnOff:
        await AppShared.setSound(false);
        isMusicSink.add(false);
        break;
    }
    _playMusic();
  }

  void _playMusic() async {
    await playSound(GameSound.Music);
    if (await AppShared.getSound()) {
      await SoundService().unMute();
    } else {
      await SoundService().mute();
    }
  }

  checkListAnswer() async {
    List<Quest> check = await AppShared.getAnswers();
    if (check.isEmpty) {
      return null;
    } else {
      return int.parse(check.last.id);
    }
  }

  checkInternet(NetworkState<QuestApplication> data) async {
    if (data.status == AppEndpoint.SUCCESS)
      await AppShared.setQuestion(data.data.questions);
  }

  checkQuestsAvailable() async {
    if (await checkListQuest()) {
      int index = await checkListAnswer();
      NetworkState<QuestApplication> data =
          new NetworkState<QuestApplication>();
      //Kiem tra xem list answer đã có chưa

      if (index != null) {
        //List answer có rồi thì lấy từ server phần tử có index+1

        data = await repository.getQuest(index + 1);
      } else {
        //List answer chưa có thì lấy từ server từ 0

        data = await repository.getQuest(0);
      }
      checkInternet(data);
      return data.message;
    }
  }

  requestQuest() async {
    String message = await checkQuestsAvailable();
    if (message != null) {
      Toast.show(message, context, duration: 3);
    }
    return message != null;
  }

  requestWifi() async {
    bool isDisconnect = await WifiService.isDisconnect();
    if (isDisconnect) {
      Toast.show(NetworkState.withDisconnect().message, context);
    }
    return isDisconnect;
  }

  playSound(GameSound gameSound) async {
    await soundService.playSound(gameSound);
  }
}
