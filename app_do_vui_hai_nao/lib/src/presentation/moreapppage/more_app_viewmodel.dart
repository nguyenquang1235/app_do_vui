import 'package:app_do_vui_hai_nao/src/resource/resource.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

import '../presentation.dart';

class MoreAppViewModel extends BaseViewModel {
  final OtherRepository repository;
  final SoundService soundService;
  NetworkState<OtherApplication> otherApplication;
  final number = 5;

  final _reducedNumber = BehaviorSubject<int>();

  Stream<int> get reducedNumberStream => _reducedNumber.stream;
  Sink<int> get reducedNumberSink => _reducedNumber.sink;

  MoreAppViewModel({@required this.repository, @required this.soundService});

  Future<NetworkState<OtherApplication>> getApps() async {
    NetworkState<OtherApplication> rs = await repository.getMoreApps();
    return rs;
  }

  playSound(GameSound gameSound) async {
    await soundService.playSound(gameSound);
  }

  Future<void> backHomeWithError() async {
    for (int i = 4; i > 0; i--) {
      await Future.delayed(Duration(seconds: 1), () {
        reducedNumberSink.add(i);
      });
    }
  }

  @override
  void dispose() {
    _reducedNumber.close();
    super.dispose();
  }
}
