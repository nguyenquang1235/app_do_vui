import 'package:app_do_vui_hai_nao/src/configs/configs.dart';
import 'package:app_do_vui_hai_nao/src/resource/model/app_model/app_model.dart';
import 'package:app_do_vui_hai_nao/src/resource/services/sounds_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:launch_review/launch_review.dart';
import 'package:loading_animations/loading_animations.dart';
import 'package:provider/provider.dart';

import '../presentation.dart';

class MoreAppScreen extends StatefulWidget {
  @override
  _MoreAppScreenState createState() => _MoreAppScreenState();
}

class _MoreAppScreenState extends State<MoreAppScreen> {
  List<Apps> _listApps;
  MoreAppViewModel _viewModel;

  @override
  void dispose() {
    _viewModel.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<MoreAppViewModel>(
        viewModel: MoreAppViewModel(
          repository: Provider.of(context),
          soundService: Provider.of(context),
        ),
        onViewModelReady: (viewModel) async {
          _viewModel = viewModel;
        },
        builder: (context, viewModel, child) {
          return _buildMoreAppPage();
        });
  }

  Widget _buildMoreAppPage() {
    return Scaffold(
      body: SafeArea(
        child: Stack(
          alignment: Alignment(1, -1),
          children: [
            Column(
              children: [
                _buildHeader(),
                Expanded(
                  child: _buildBody(),
                ),
              ],
            ),
            WidgetGameButton(
              onTap: () async {
                await _viewModel.playSound(GameSound.Back);
                Navigator.pop(context, true);
              },
              normal: Image.asset(
                AppImages.btnBack1,
                width: WidgetConfigSize.blockSizeHorizontal * 15,
                height: WidgetConfigSize.blockSizeHorizontal * 15,
              ),
              hover: Image.asset(
                AppImages.btnBack2,
                width: WidgetConfigSize.blockSizeHorizontal * 15,
                height: WidgetConfigSize.blockSizeHorizontal * 15,
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildHeader() {
    return Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(AppImages.gameBar2), fit: BoxFit.fill)),
        child: Center(
          child: Image.asset(
            AppImages.moreApp,
            width: WidgetConfigSize.blockSizeHorizontal * 70,
            height: WidgetConfigSize.blockSizeVertical * 18,
          ),
        ));
  }

  Widget _buildBody() {
    return Container(
      // padding: const EdgeInsets.only(top: 8),
      decoration: const BoxDecoration(
          image: DecorationImage(
              image: AssetImage(AppImages.background3), fit: BoxFit.fill)),
      child: FutureBuilder(
        future: _viewModel.getApps(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return WidgetLoadingPage();
          } else {
            if (snapshot.data.message == null) {
              _listApps = snapshot.data.data.apps;
              return _buildListMoreApp();
            } else {
              return backHomeWhenError();
            }
          }
        },
      ),
    );
  }

  Widget _buildListMoreApp() {
    return ListView.separated(
      physics: BouncingScrollPhysics(),
      separatorBuilder: (_, __) => Container(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: WidgetSeparation(
            height: 20,
            color: Colors.brown,
          )),
      padding: EdgeInsets.only(bottom: 48),
      itemCount: _listApps.length,
      itemBuilder: (context, index) {
        return _buildAppItem(_listApps[index], index);
      },
    );
  }

  Widget _buildAppItem(Apps apps, int index) {
    return Container(
      padding: index == 0 ?  const EdgeInsets.only(top: 8) : null,
      child: ListTile(
        leading: Image.network(
          apps.logolink,
          loadingBuilder: (context, child, loadingProgress) {
            if (loadingProgress == null) return child;
            return LoadingBouncingLine.circle(
              size: 50,
              backgroundColor: Colors.white,
            );
          },
          fit: BoxFit.fill,
          height: 60,
          width: 60,
        ),
        title: Text(
          apps.name,
          style: AppStyles.DEFAULT_MEDIUM_BOLD
              .copyWith(color: Colors.brown, fontFamily: "Itim"),
        ),
        trailing: AspectRatio(
          aspectRatio: 2,
          child: WidgetGameButton(
            onTap: () async {
              await _viewModel.playSound(GameSound.Back);
              LaunchReview.launch(
                  androidAppId: apps.linkdown, iOSAppId: apps.linkdownios);
            },
            normal: Image.asset(AppImages.download1),
            hover: Image.asset(AppImages.download2),
          ),
        ),
      ),
    );
  }

  Widget backHomeWhenError() {
    String message = "Kết nối bị lỗi, trở về trong";
    TextStyle style = AppStyles.DEFAULT_LARGE_BOLD
        .copyWith(color: Colors.brown, fontFamily: "Poppins");
    _viewModel.backHomeWithError().whenComplete(() async =>
        await Future.delayed(
            Duration(milliseconds: 500), () => Navigator.pop(context)));
    return StreamBuilder(
      stream: _viewModel.reducedNumberStream,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(
            child: Text(
              "$message 5 giây",
              style: style,
            ),
          );
        } else {
          return Center(
            child: Text(
              "$message ${snapshot.data.toString()} giây",
              style: style,
            ),
          );
        }
      },
    );
  }
}
