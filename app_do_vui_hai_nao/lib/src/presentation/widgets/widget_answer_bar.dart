import 'package:app_do_vui_hai_nao/src/configs/configs.dart';
import 'package:app_do_vui_hai_nao/src/presentation/presentation.dart';
import 'package:flutter/material.dart';

typedef void ParamFunction(bool value);

class WidgetAnswerBar extends StatefulWidget {
  String choose; // A B C D
  String answerBar = AppImages.answerBar1; //Red
  String correct = AppImages.answerBar2;
  String incorrect = AppImages.answerBar3;
  String text;
  String answerCorrect;
  ParamFunction paramFunction;
  bool correctResult;
  String showCorrect;

  @override
  _WidgetAnswerBarState createState() => _WidgetAnswerBarState();
  WidgetAnswerBar(
      {this.choose,
      this.text,
      this.answerCorrect,
      this.paramFunction,
      this.showCorrect});
}

class _WidgetAnswerBarState extends State<WidgetAnswerBar>{
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    bool checkCorrect =
        widget.text.toLowerCase() == widget.answerCorrect.toLowerCase();
    if (widget.showCorrect != null && widget.showCorrect.toLowerCase() == widget.text.toLowerCase()) {
      setState(() {
        widget.answerBar = AppImages.answerBar2;
      });
    }
    return Container(
      child: Stack(
        children: [
          Align(
            alignment: Alignment(0, -0.2),
            child: WidgetGameButton(
              onTap: () async {
                widget.paramFunction(checkCorrect);
              },
              normal: _buildWidgetAnswerText(widget.answerBar),
              hover: _buildWidgetAnswerText(widget.incorrect),
            ),
          ),
          AspectRatio(
            aspectRatio: 1,
            child: Image.asset(
              widget.choose,
              fit: BoxFit.fitHeight,
            ),
          )
        ],
      ),
    );
  }

  Widget _buildWidgetAnswerText(String answerBar) {
    double ratio = MediaQuery.of(context).size.width > 500 ? 12 : 7;
    // build thanh trả lời cùng với text
    return AspectRatio(
      aspectRatio: ratio,
      child: Container(
        margin: const EdgeInsets.only(left: 16),
        child: Row(
          children: [
            Expanded(
              child: Opacity(
                opacity: 0,
                child: Text("ádasdasd"),
              ),
              flex: 1,
            ),
            Expanded(
              child: Text(
                widget.text,
                style:
                    AppStyles.DEFAULT_MEDIUM_BOLD.copyWith(color: Colors.white),
              ),
              flex: 5,
            )
          ],
        ),
        decoration: BoxDecoration(
          image:
              DecorationImage(image: AssetImage(answerBar), fit: BoxFit.fill),
        ),
      ),
    );
  }

  // Future<void>_onPressButton(bool checkCorrect) async {
  //   if(!onPress)
  //     widget.paramFunction(checkCorrect);
  //   onPress = true;
  // }
}
