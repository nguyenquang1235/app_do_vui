import 'package:app_do_vui_hai_nao/src/configs/configs.dart';
import 'package:flutter/material.dart';
import 'package:loading_animations/loading_animations.dart';
class WidgetLoadingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
          image: const DecorationImage(
              image: AssetImage(AppImages.background3), fit: BoxFit.fill)),
      child: Center(
        child: LoadingBouncingLine.circle(
          size: 100,
          backgroundColor: Colors.white,
        ),
      ),
    );
  }
}
