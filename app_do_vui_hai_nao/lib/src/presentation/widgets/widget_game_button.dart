import 'package:flutter/material.dart';

typedef void VoidFuntion();

class WidgetGameButton extends StatefulWidget {
  Widget normal;
  Widget hover;
  VoidFuntion onTap;
  @override
  _WidgetGameButtonState createState() => _WidgetGameButtonState();
  WidgetGameButton({this.normal, this.hover, this.onTap});
}

class _WidgetGameButtonState extends State<WidgetGameButton> {
  bool isHover;
  bool onPress = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    isHover = false;
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: GestureDetector(
        onTap: () async {
          if(!onPress){
            widget.onTap();
            onPress = true;
          }
          await Future.delayed(Duration(seconds: 1), (){
            onPress = false;
          });
        },
        child: isHover ? widget.hover : widget.normal,
        onTapUp: (details) {
          setState(() {
            isHover = false;
          });
        },
        onTapDown: (details) {
          setState(() {
            isHover = true;
          });
        },
        onTapCancel: () => setState(() {
          isHover = false;
        }),
      ),
    );
  }
}
