import 'package:app_do_vui_hai_nao/src/presentation/presentation.dart';
import 'package:flutter/material.dart';

class WidgetTimeIndicator extends StatefulWidget {
  final int time;
  const WidgetTimeIndicator({Key key, this.time}) : super(key: key);
  @override
  _WidgetTimeIndicatorState createState() => _WidgetTimeIndicatorState();
}

class _WidgetTimeIndicatorState extends State<WidgetTimeIndicator> with SingleTickerProviderStateMixin {
  AnimationController _timeController;

  @override
  void dispose() {
    // TODO: implement dispose
    _timeController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _timeController = new AnimationController(
        vsync: this, duration: Duration(seconds: widget.time == null||widget.time <= 0 ? 5 : widget.time));
    _timeController.forward();
    // _timeController.addListener(() {
    //   if(_timeController.status == AnimationStatus.completed){
    //     print(true);
    //     _timeController.reset();
    //     _timeController.forward();
    //   }
    // });
  }

  @override
  Widget build(BuildContext context) {
    WidgetConfigSize().init(context);
    Animation<double> animation =
        CurvedAnimation(curve: Curves.easeIn, parent: _timeController);
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 2.5, vertical: 1),
      width: WidgetConfigSize.blockSizeHorizontal * 27,
      height: WidgetConfigSize.blockSizeVertical * 1.4,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
      child: AnimatedBuilder(
        key: UniqueKey(),
        animation: animation,
        builder: (context, child) => ClipPath(
          clipper: WidgetCustomClipper(),
          child: LinearProgressIndicator(
            value: 1-animation.value,
            backgroundColor: Colors.transparent,
            valueColor: ColorTween(
              begin: Colors.yellow,
              end: Colors.red,
            ).animate(_timeController),
          )
        ),
      ),
    );
  }
}
