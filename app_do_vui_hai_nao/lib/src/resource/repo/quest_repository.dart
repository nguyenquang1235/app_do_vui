import 'dart:async';
import 'package:app_do_vui_hai_nao/src/configs/configs.dart';
import 'package:app_do_vui_hai_nao/src/utils/app_clients.dart';
import 'package:dio/dio.dart';

import '../resource.dart';

class QuestRepository {
  static const base = "relax365.net";

  ///http://relax365.net/hsdovuihainao?id=
  Future<NetworkState<QuestApplication>> getQuest(int id) async {
    bool isDisconnect = await WifiService.isDisconnect();
    if(isDisconnect) return NetworkState.withDisconnect();
    try{
      Response response;
      Map<String, String> param = {"id": id.toString()};
      // response = await Api().get(AppEndpoint.APP, param);
      response = await AppClients().get(AppEndpoint.APP, queryParameters: param);
      return NetworkState(
          status: AppEndpoint.SUCCESS,
          data: QuestApplication.fromJson(response.data));
    }on DioError catch(e){
      return NetworkState.withError(e);
    }
  }
}
