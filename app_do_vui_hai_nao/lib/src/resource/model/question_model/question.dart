class Quest {
  String _id;
  String _cauHoi;
  String _a;
  String _b;
  String _c;
  String _d;
  String _giaiThich;
  String _level;

  String get id => _id;
  String get cauHoi => _cauHoi;
  String get a => _a;
  String get b => _b;
  String get c => _c;
  String get d => _d;
  String get giaiThich => _giaiThich;
  String get level => _level;

  Quest({
    String id,
    String cauHoi,
    String a,
    String b,
    String c,
    String d,
    String giaiThich,
    String level,
  }) {
    _id = id;
    _cauHoi = cauHoi;
    _a = a;
    _b = b;
    _c = c;
    _d = d;
    _giaiThich = giaiThich;
    _level = level;
  }
  factory Quest.fromJson(Map<String, dynamic> json) {
    return Quest(
      id: json['ID'],
      cauHoi: json['CauHoi'],
      a: json['A'],
      b: json['B'],
      c: json['C'],
      d: json['D'],
      giaiThich: json['GiaiThich'],
      level: json['level'],
    );
  }

  Map<String, dynamic> toJson(){
    var map = <String, dynamic>{};
    map['ID'] = _id;
    map['CauHoi'] =_cauHoi;
    map['A'] = _a;
    map['B'] = _b;
    map['C'] = _c;
    map['D'] = _d;
    map['GiaiThich'] = _giaiThich;
    map['level'] = _level;
    return map;
  }
}
