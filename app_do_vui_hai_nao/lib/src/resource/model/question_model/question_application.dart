import 'package:app_do_vui_hai_nao/src/resource/model/model.dart';

class QuestApplication {
  List<Quest> _questions;

  List<Quest> get questions => _questions;

  QuestionApplication({List<Quest> questions}) {
    _questions = questions;
  }

  QuestApplication.fromJson(List<dynamic> data) {
    _questions = [];
    for(var item in data){
      _questions.add(Quest.fromJson(item));
    }
  }
}
