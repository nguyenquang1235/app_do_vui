import 'package:app_do_vui_hai_nao/src/presentation/presentation.dart';
import 'package:app_do_vui_hai_nao/src/resource/resource.dart';
import 'package:app_do_vui_hai_nao/src/resource/services/sounds_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';

void main() async {
  SoundService soundService = new SoundService();
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setEnabledSystemUIOverlays([]);
  WidgetsBinding.instance.addObserver(soundService);
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(MultiProvider(child: MyApp(), providers: <SingleChildWidget>[
      Provider.value(value: OtherRepository()),
      Provider.value(value: QuestRepository()),
      Provider.value(value: soundService..init()),
    ]));
  });
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      onGenerateRoute: Routers.generateRoute,
      initialRoute: Routers.homePage,
      title: '',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
    );
  }
}
